# BlogMaker Lite

This project is my repo for following the [Django from First Principles](https://www.mostlypython.com/django-from-first-principles/) series, by [Eric Matthes](https://www.mostlypython.com/author/eric/). I liked his concept of working through learning Django from an empty directory, instead of through the default framework setup. The default framework setup is great, but there's a lot to unpack there in order to understand it as a complete noob (like me).  

## To Do

- [x] Complete [Django from first principles, part 2](https://www.mostlypython.com/django-from-first-principles-part-2/)
- [ ] Complete [Django from first principles, part 3](https://www.mostlypython.com/django-from-first-principles-part-3/)
